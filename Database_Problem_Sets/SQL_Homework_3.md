# SQL Intermediate Homework Set 3

## Exercise 1

Write the following queries in SQL, using the University schema.

a) Find the IDs of all students who were taught by an instructor named Einstein; make sure there are no duplicates in the result.

```sql
SELECT DISTINCT STUDENT.ID
FROM STUDENT, INSTRUCTOR, TAKES, TEACHES
WHERE  STUDENT.ID = TAKES.ID AND TAKES.COURSE_ID = TEACHES.COURSE_ID
AND TEACHES.ID = INSTRUCTOR.ID AND INSTRUCTOR.NAME = 'Einstein';
```

b) Find all instructors earning the highest salary (there may be more than one with the same salary).

```sql
SELECT NAME, SALARY
FROM INSTRUCTOR
WHERE SALARY = (
  SELECT MAX(SALARY)
  FROM INSTRUCTOR);
```

c) Find the enrolment of each section that was offered in Autumn 2009.

```sql
SELECT DISTINCT COUNT(t.COURSE_ID) AS 'Enrolment', c.TITLE AS 'Course'
FROM STUDENT AS s, TAKES AS t, COURSE AS c
WHERE s.ID = t.ID AND t.SEMESTER = 'FALL' AND t.YEAR = 2009 AND c.COURSE_ID = t.COURSE_ID
GROUP BY c.TITLE;
```

d) Find the maximum enrolment, across all sections, in Autumn 2009.

```sql
SELECT DISTINCT COUNT(t.COURSE_ID) AS 'Max Enrollment', c.TITLE AS 'Course'
FROM STUDENT AS s, TAKES AS t, COURSE AS c
WHERE s.ID = t.ID AND t.SEMESTER = 'FALL' AND t.YEAR = 2009 AND c.COURSE_ID = t.COURSE_ID
GROUP BY c.TITLE
ORDER BY 'Max Enrollment' DESC
LIMIT 1;
```

## Exercise 2

Suppose you are given a relation *grade_points* (grade, points), which provides a conversion from letter grades in the takes relation to numeric scores;
for example, an “A” grade could be specified to correspond to 4 points, an “A−” to 3.7 points, a “B+” to 3.3 points, a “B” to 3 points, and so on.
The grade points earned by a student for a course offering (section) is defined as the number of credits for the course multiplied by the numeric points for the grade that the student received.
Given the above relation, and our university schema, write each of the following queries in SQL. You can assume for simplicity that no takes tuple has the null value for grade.

a) Find the total grade-points earned by the student with ID 12345, across all courses taken by the student.

```sql
SELECT NAME, TOT_CRED AS "Total Grade Points"
FROM STUDENT
WHERE STUDENT.ID = 12345;
```

b) Find the grade-point average (GPA) for the above student, that is, the total grade-points divided by the total credits for the associated courses.

First, add a grade_score column to the takes table.

```sql
alter table takes add grade_score REAL;
UPDATE takes SET grade_score = 4.0 WHERE grade = 'A';
UPDATE takes SET grade_score = 3.7 WHERE grade = 'A-';
UPDATE takes SET grade_score = 3.3 WHERE grade = 'B+';
UPDATE takes SET grade_score = 3.0 WHERE grade = 'B';
```

and so on. Then we can use this column to work out the GPA.

```sql
SELECT s.ID AS 'Student ID', s.NAME AS 'Name', ROUND(SUM(c.CREDITS*t.GRADE_SCORE)/s.TOT_CRED,2) AS 'GPA'
FROM COURSE AS c, TAKES AS t, STUDENT AS s
WHERE t.COURSE_ID = c.COURSE_ID AND s.ID = t.ID AND s.ID = 12345
GROUP BY s.ID;
```

c) Find the ID and the grade-point average of every student.

```sql
SELECT s.ID AS 'Student ID', s.NAME AS 'Name', ROUND(SUM(c.CREDITS*t.GRADE_SCORE)/s.TOT_CRED,2) AS 'GPA'
FROM COURSE AS c, TAKES AS t, STUDENT AS s
WHERE t.COURSE_ID = c.COURSE_ID AND s.ID = t.ID
GROUP BY s.ID;
```

## Exercise 3

1) Write the following queries in SQL.

a) Display a list of all instructors, showing their ID, name, and the number of sections that they have taught. Make sure to show the number of sections as 0 for instructors who have not taught any section.

```sql
SELECT i.ID, i.NAME, COUNT(*)
FROM INSTRUCTOR AS i, TEACHES AS t
WHERE i.ID=t.ID
GROUP BY i.ID, i.NAME;
```

b) Display the list of all departments, with the total number of instructors in each department. Make sure to correctly handle departments with no instructors.

```sql
SELECT d.dept_name, COUNT(*) AS 'Number of Instructors'
FROM DEPARTMENT AS d, INSTRUCTOR AS i
WHERE d.dept_name = i.dept_name
GROUP BY d.dept_name;
```

2) Outer join expressions can be computed in SQL without using the SQL outer join operation. To illustrate this fact, show how to rewrite each of the following SQL queries without using the outer join expression.

a) select * from student natural left outer join takes.

```sql
SELECT *
FROM STUDENT AS s, TAKES AS t
WHERE t.ID = s.ID;
```
