# SQL Beginner Homework Set 1

## Exercise 1

Specify the following queries in SQL on the provided COMPANY relational database schema shown in the figure below.
Specify the following queries in SQL on this schema:

a. Retrieve the names of all employees in department 5 who work more than 10 hours per week on the “Product Mega” project.

```sql
SELECT e.FNAM, e.LNAME
FROM EMPLOYEE AS e, DEPARTMENT AS d, PROJECT AS p, WORKS_ON AS w
WHERE e.NI = w.ENI AND w.PNO = p.PNUM
  AND p.PNAME = "Product Mega" AND d.DUM = 5 AND w.HOURS > 10;
```

b. Show the resulting salaries if every employee working on the ‘Product Alfa’ project is given a 10% raise.

```sql
SELECT e.FNAM, e.LNAME, (e.SALARY)*1.1
FROM EMPLOYEE AS e, PROJECT AS p, WORKS_ON AS w
WHERE e.NI = w.ENI AND p.PNUM = w.PNO AND p.PNAME = "Product Alfa";
```

c. List the names of all employees who have a dependent with the same first name as themselves.

```sql
SELECT e.FNAM, e.LNAME
FROM EMPLOYEE AS e, DEPENDENT AS d,
WHERE (e.FNAM + ' ' + e.LNAME) = d.DEPENDENT_NAME AND e.NI = d.ENI;
```

d. Retrieve a list of employees and the projects they are working on, ordered by department and, within each department, alphabetically by last name, first name.

```sql
SELECT e.FNAM, e.LNAME, p.PNAME
FROM EMPLOYEE AS e, PROJECT AS p, DEPARTMENT AS d, WORKS_ON AS w
WHERE e.NI = w.ENI AND w.PNO = p.PNUM
ORDER BY d.DNAME, e.LNAME, e.FNAM;
```

e. Find the names of all employees who are directly supervised by ‘Franklin Wong’.

```sql
SELECT e.FNAM, e.LNAME
FROM EMPLOYEE AS e, employee AS m
WHERE e.SUPERNI = m.MGRNI AND m.FNAM = 'Franklin' AND m.LNAME = 'Wong';
```

## Exercise 2

a. Retrieve the names of all senior students majoring in ‘CS’ (computer science).

```sql
SELECT s.Name
FROM STUDENT AS s
WHERE s.Major = 'CS' AND s.Class = 4;
```

b. Retrieve the names of all courses taught by Professor King in 2007 and 2008.

```sql
SELECT c.Course_name
FROM COURSE AS c, SECTION AS s,
WHERE c.Course_number = s.Course_number AND s.Instructor = "King" AND (s.Year = 07 OR s.Year = 08);
```

c. For each section taught by Professor King, retrieve the course number, semester, year,
and number of students who took the section.

```sql
SELECT s.Course_number, s.Semester, s.Year, COUNT(p.Name)
FROM SECTION AS s, GRADE_REPORT AS g, STUDENT AS p
WHERE s.Section_identifier = g.Section_identifier AND g.Student_number = p.Student_number AND s.Instructor = "King";
```

d. Retrieve the name and transcript of each senior student (Class = 4) majoring in CS. A
transcript includes course name, course number, credit hours, semester, year, and grade
for each course completed by the student.

```sql
SELECT s.Name, c.Course_name, c.Course_number, c.Credit_hours, s.Semester, s.Year, g.Grade
FROM EMPLOYEE AS e, COURSE AS c, SECTION AS s, GRADE_REPORT AS g
WHERE g.Student_number = s.Student_number AND g.Section_identifier = s.Section_identifier AND s.Course_number = c.Course_number;
```

e. For all instructors who have taught some course, find their names and the course ID
and name of the courses they taught

```sql
SELECT s.Instructor, c.Course_number, c.Course_name
FROM SECTION AS s, COURSE AS c
WHERE s.Course_number = c.Course_number;
```
