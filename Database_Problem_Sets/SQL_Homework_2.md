# SQL Homework Set 2

## Exercise 2

2.

### Using the Company DB.

a) Count the number of distinct salary values in the database.

```sql
SELECT DISTINCT COUNT(EMPLOYEE.SALARY) FROM EMPLOYEE;
```

b) Find the names and average salaries of all departments whose average salary is greater than 42000.

```sql
SELECT DEPARTMENT.DNAME, ROUND(AVG(EMPLOYEE.SALARY),2)
FROM EMPLOYEE, DEPARTMENT
WHERE EMPLOYEE.DNO = DEPARTMENT.DNUM
GROUP BY DEPARTMENT.DNAME
HAVING AVG(EMPLOYEE.SALARY) > 4200;
```

c) For each department, retrieve the department number, the number of employees in the department, and their average salary.

```sql
SELECT DEPARTMENT.DNUM AS 'Department Number', COUNT(EMPLOYEE.FNAME) AS 'Number of Employees', ROUND(AVG(EMPLOYEE.SALARY),2) AS 'Average Salary'
FROM EMPLOYEE, DEPARTMENT
WHERE EMPLOYEE.DNO = DEPARTMENT.DNUM
GROUP BY DEPARTMENT.DNUM;
```

d) For each project, retrieve the project number, the project name, and the number of employees who work on that project.

```sql
SELECT PROJECT.PNUM, PROJECT.PNAME, COUNT(WORKS_ON.ENI)
FROM WORKS_ON, PROJECT
WHERE WORKS_ON.PNO = PROJECT.PNUM
GROUP BY PROJECT.PNUM;
```

e) For each project on which more than two employees work, retrieve the project number, the project name, and the number of employees who work on the project.

```sql
SELECT PROJECT.PNUM, PROJECT.PNAME, COUNT(WORKS_ON.ENI)
FROM WORKS_ON, PROJECT
WHERE WORKS_ON.PNO = PROJECT.PNUM GROUP BY
PROJECT.PNUM
HAVING COUNT(WORKS_ON.ENI) > 2;
```

f) Find the sum of the salaries of all employees of the ‘Research’ department, as well as the maximum salary, the minimum salary, and the average salary in this
department.

```sql
SELECT ROUND(SUM(EMPLOYEE.SALARY),2) AS 'Total Salary', ROUND(MAX(EMPLOYEE.SALARY),2) AS 'Max Salary',
	ROUND(MIN(EMPLOYEE.SALARY),2) AS 'Min Salary', ROUND(AVG(EMPLOYEE.SALARY),2) AS 'Average Salary'
FROM EMPLOYEE, DEPARTMENT
WHERE EMPLOYEE.DNO = DEPARTMENT.DNUM AND DEPARTMENT.DNAME = 'research';
```

g) Retrieve the total number of employees in the company and the number of employees in the ‘Research’ department.

```sql
SELECT COUNT(EMPLOYEE.FNAME) AS 'Number of Employees'
FROM EMPLOYEE, DEPARTMENT
WHERE EMPLOYEE.DNO = DEPARTMENT.DNUM
  AND (DEPARTMENT.DNAME IS ANY AND DEPARTMENT.DNAME = 'research');
```

h) Retrieve the national insurance numbers of all employees who work on project number 1, 2, or 3.

```sql
SELECT e.NI
FROM EMPLOYEE AS e, WORKS_ON AS w
WHERE e.NI = w.ENI AND w.PNO IN (1,2,3);
```

i) For each department having more than five employees, retrieve the department number and the number of employees making more than £40,000.

```sql
SELECT DEPARTMENT.DNAME AS 'Department Name', COUNT(EMPLOYEE.FNAME) AS 'Number of Employees'
FROM DEPARTMENT, EMPLOYEE
WHERE EMPLOYEE.DNO = DEPARTMENT.DNUM AND EMPLOYEE.SALARY > 4000
GROUP BY DEPARTMENT.DNAME
HAVING COUNT(EMPLOYEE.FNAME) > 5;
```

### Using the University DB

a) Find the average salary of instructors in the Computer Science department.

```sql
SELECT ROUND(AVG(INSTRUCTOR.SALARY),2)
FROM INSTRUCTOR, DEPARTMENT
WHERE DEPARTMENT.DEPT_NAME = 'COMP. SCI.';
```

b) Find the total number of instructors who teach a course in the Spring 2010 semester.

```sql
SELECT COUNT(i.ID) AS 'Number of Instructors'
FROM INSTRUCTOR AS i, COURSE AS c, TAKES AS t
WHERE t.YEAR = 2010 AND t.SEMESTER = 'SPRING'
	AND t.COURSE_ID = c.COURSE_ID AND c.DEPT_NAME = i.DEPT_NAME;
```
